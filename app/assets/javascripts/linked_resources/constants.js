import { __ } from '~/locale';

export const LINKED_RESOURCES_HEADER_TEXT = __('Linked resources');
export const LINKED_RESOURCES_HELP_TEXT = __('Read more about linked resources');
export const LINKED_RESOURCES_ADD_BUTTON_TEXT = __('Add a resource link');
